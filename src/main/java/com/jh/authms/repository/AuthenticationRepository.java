package com.jh.authms.repository;

import com.jh.authms.model.AuthorityEnum;
import com.jh.authms.model.entity.AuthorityEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Set;

public interface AuthenticationRepository extends JpaRepository<AuthorityEntity,Long> {
    Set<AuthorityEntity>findAllByAuthorityIn(Set<AuthorityEnum>auths);
}
