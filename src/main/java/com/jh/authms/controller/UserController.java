package com.jh.authms.controller;

import com.jh.authms.model.dto.CreateUserDto;
import com.jh.authms.model.response.UserResponse;
import com.jh.authms.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {
    private final UserService service;

    @GetMapping("/demo")
    public String demo(){
        SecurityContext context = SecurityContextHolder.getContext();
        Authentication authentication = context.getAuthentication();
        return "username: " + authentication.getName() +"\n" +
                "authorities: " + authentication.getAuthorities() +"\n" +
                "credentials: " + authentication.getCredentials() +"\n" +
                "isAuthenticated: "+authentication.isAuthenticated();
    }

    @PostMapping
    public ResponseEntity<UserResponse>createUser(@RequestBody CreateUserDto request){
        return ResponseEntity.ok(service.createUser(request));
    }


}
