package com.jh.authms.mapstruct;

import com.jh.authms.model.AuthorityEnum;
import com.jh.authms.model.dto.CreateUserDto;
import com.jh.authms.model.dto.GrantedAuthorityImpl;
import com.jh.authms.model.dto.UserDetailsImpl;
import com.jh.authms.model.entity.AuthorityEntity;
import com.jh.authms.model.entity.UserEntity;
import com.jh.authms.model.response.UserResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface UserMapstruct {
    UserEntity mapCreateUserDtoToEntity(CreateUserDto request);
    @Mapping(target = "authorities", source = "authorities", qualifiedByName = "authToEnum")
    UserResponse mapEntityToUserResponse(UserEntity user);
    @Named("authToEnum")
    default Set<AuthorityEnum> authToEnum(Set<AuthorityEntity>entities){
        return entities.stream()
                .map(AuthorityEntity::getAuthority)
                .collect(Collectors.toSet());
    }
}
