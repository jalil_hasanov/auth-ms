package com.jh.authms.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.HashMap;
import java.util.Map;

public enum AuthorityEnum {
    READ("READ"),WRITE("WRITE"),UPDATE("UPDATE");

    private final String val;

    AuthorityEnum(final String val) {
        this.val = val;
    }

    @JsonValue
    public String getVal() {
        return val;
    }

    private static final Map<String, AuthorityEnum> AUTHORITY_ENUM_MAP = new HashMap<>();

    static {
        AUTHORITY_ENUM_MAP.put("READ", READ);
        AUTHORITY_ENUM_MAP.put("WRITE", WRITE);
        AUTHORITY_ENUM_MAP.put("UPDATE", UPDATE);
    }

    @JsonCreator
    public static AuthorityEnum fromValue(final String val) {
        return AUTHORITY_ENUM_MAP.get(val.toUpperCase());
    }

}
