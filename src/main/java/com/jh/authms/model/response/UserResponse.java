package com.jh.authms.model.response;

import com.jh.authms.model.AuthorityEnum;

import java.util.List;
import java.util.Set;

public record UserResponse(
        String name,
        String lastName,
        String username,
        String email,
        Set<AuthorityEnum> authorities
) {
}
