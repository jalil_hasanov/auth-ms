package com.jh.authms.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jh.authms.model.AuthorityEnum;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@Table(name = "auth")
public class AuthorityEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "auth_id", nullable = false)
    Long authId;
    @Enumerated(EnumType.STRING)
    @Column(name = "authority", unique = true)
    AuthorityEnum authority;
    @ManyToMany(mappedBy = "authorities", cascade = CascadeType.MERGE)
    @EqualsAndHashCode.Exclude
    Set<UserEntity> users = new HashSet<>();
}
