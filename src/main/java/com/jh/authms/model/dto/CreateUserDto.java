package com.jh.authms.model.dto;

import com.jh.authms.model.AuthorityEnum;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.List;
import java.util.Set;

public record CreateUserDto(
        String name,
        String lastName,
        String username,
        String email,
        String password,
        @Enumerated(EnumType.STRING)
        Set<AuthorityEnum> authorities
) {
    @Override
    public String password() {
        return new BCryptPasswordEncoder().encode(password);
    }
}
