package com.jh.authms.model.dto;

import com.jh.authms.model.entity.AuthorityEntity;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.security.core.GrantedAuthority;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class GrantedAuthorityImpl implements GrantedAuthority {
    AuthorityEntity authority;

    @Override
    public String getAuthority() {
        return authority.getAuthority().getVal();
    }
}
