package com.jh.authms.service;

import com.jh.authms.model.dto.CreateUserDto;
import com.jh.authms.model.response.UserResponse;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {
    UserResponse createUser(CreateUserDto request);
}
