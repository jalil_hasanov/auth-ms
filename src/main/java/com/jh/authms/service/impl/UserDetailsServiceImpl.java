package com.jh.authms.service.impl;

import com.jh.authms.mapstruct.UserMapstruct;
import com.jh.authms.model.AuthorityEnum;
import com.jh.authms.model.dto.CreateUserDto;
import com.jh.authms.model.dto.UserDetailsImpl;
import com.jh.authms.model.entity.AuthorityEntity;
import com.jh.authms.model.entity.UserEntity;
import com.jh.authms.model.response.UserResponse;
import com.jh.authms.repository.AuthenticationRepository;
import com.jh.authms.repository.UserRepository;
import com.jh.authms.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final AuthenticationRepository authRepository;
    private final UserMapstruct mapstruct;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserEntity user = userRepository.findUserEntityByUsername(username)
                .orElseThrow(()->new UsernameNotFoundException("User: "+username+" could not be found."));
        UserDetailsImpl userDetails = UserDetailsImpl.builder()
                .user(user)
                .build();
        return userDetails;
    }

    @Override
    public UserResponse createUser(CreateUserDto request) {
        UserEntity entity = mapstruct.mapCreateUserDtoToEntity(request);
        Set<AuthorityEntity>authorityEntities = authRepository.findAllByAuthorityIn(request.authorities());
        entity.setAuthorities(authorityEntities);
        UserEntity user = userRepository.save(entity);
        return mapstruct.mapEntityToUserResponse(user);
    }
}
