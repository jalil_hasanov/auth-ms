FROM arm64v8/openjdk:17.0.1-jdk-oracle
LABEL maintainer="jhasanov2019@gmail.com"
VOLUME /main-app
COPY build/libs/auth-ms-0.0.1-SNAPSHOT.jar app.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","app.jar"]

#RUN mkdir "/app"
#COPY build/libs/study-0.0.1-SNAPSHOT.jar /app/study-0.0.1-SNAPSHOT.jar
#WORKDIR /app
#EXPOSE 8080
#CMD ["java","-jar","study-0.0.1-SNAPSHOT.jar"]